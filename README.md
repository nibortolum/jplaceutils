# Jplace Utils

A collection of R scripts to process and filter Jplace files

# Requirements
- R statistical Software
- Packages
    - rjson
    - optparse

# files

## filter_jplace.R

Takes a jplace as Input, and filter the placements by probability threshold. Only the placements with a probability higher than the treshold are kept in the output file. To see the help, type :

`Rscript filter_place.R -h`
